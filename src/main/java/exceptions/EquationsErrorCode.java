package exceptions;

public enum EquationsErrorCode {
    EQUATION_NOT_QUADRATIC("The equation should be quadratic (a != 0)");
    private String exception;

    EquationsErrorCode(String exception) {
        setException(exception);
    }

    private void setException(String exception) {
        this.exception = exception;
    }

    public String getException() {
        return exception;
    }
}
