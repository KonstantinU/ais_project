package exceptions;

public class EquationException extends Exception {
    private EquationsErrorCode errorCode;

    public EquationException(EquationsErrorCode errorCode) {
        super(errorCode.getException());
        setErrorCode(errorCode);
    }

    public EquationException(String message) {
        this(EquationsErrorCode.valueOf(message));
    }

    public EquationsErrorCode getErrorCode() {
        return errorCode;
    }

    private void setErrorCode(EquationsErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
