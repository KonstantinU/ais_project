package equations;

import exceptions.EquationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuadraticTest {
    private final double eps = 1e-6;

    @Test
    void solveQuadOneRoot() throws EquationException {
        Quadratic q = new Quadratic();
        double[] x = q.solve(1., -2., 1.);
        assertEquals(x.length, 1);
        assertEquals(x[0], 1., eps);
    }

    @Test
    void solveQuadTwoRoots() throws EquationException {
        Quadratic q = new Quadratic();
        double[] x = q.solve(1., 0., -1.);
        assertEquals(x.length, 2);
        assertEquals(x[0], 1., eps);
        assertEquals(x[1], -1., eps);
    }

    @Test
    void solveQuadZeroRoots() throws EquationException {
        Quadratic q = new Quadratic();
        double[] x = q.solve(1., 0., 1.);
        assertEquals(x.length, 0);
    }

    @Test
    void solveQuadThrowsException() {
        Quadratic q = new Quadratic();
        assertThrows(EquationException.class, () -> q.solve(0., 2., 1.));
    }

}