package equations;

import exceptions.EquationException;
import exceptions.EquationsErrorCode;

public class Quadratic {
    private double eps;

    public Quadratic() {
        this(1e-6);
    }

    public Quadratic(double eps) {
        setEps(eps);
    }

    public double getEps() {
        return eps;
    }

    public void setEps(double eps) {
        this.eps = eps;
    }

    private boolean isNear(double a1, double a2) {
        return Math.abs(a1 - a2) < eps;
    }

    public double[] solve(double a, double b, double c) throws EquationException {
        if(isNear(a, 0.0)) {
            throw new EquationException(EquationsErrorCode.EQUATION_NOT_QUADRATIC);
        }
        double D = b*b - 4 * a * c;
        double[] x;
        if(D > 0) {
            x = new double[2];
            x[0] = (- b + Math.sqrt(D)) / (2 * a);
            x[1] = (- b - Math.sqrt(D)) / (2 * a);
        } else if (D == 0) {
            x = new double[1];
            x[0] = - b / (2 * a);
        } else {
            x = new double[0];
        }
        return x;
    }
}
