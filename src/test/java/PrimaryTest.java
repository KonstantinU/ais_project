import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PrimaryTest {
    @Test
    void testMath() {
        assertEquals(2 + 2, 4);
        assertEquals(2 + 5, 7);
    }
}
